import time
from datetime import date

## CLASES ##

class Parquimetro():
    def __init__(self, __id_parquimetro):
        self.__id_parquimetro = __id_parquimetro
        self.__patente_actual = ""
        self.__minutos_estacionado = 0
        self.__auto_estacionado = False
    
    def auto_detectado(self):
        self.__auto_estacionado = True

    def estacionamiento_finalizado(self):
        self.__auto_estacionado = False

    def set_id_parquimetro(self, __id_parquimetro):
        self.__id_parquimetro = __id_parquimetro

    def get_id_parquimetro(self):
        return self.__id_parquimetro

    def set_patente_actual(self, patente_actual):
        self.__patente_actual = patente_actual

    def get_patente_actual(self):
        return self.__patente_actual
    
    def set_minutos_estacionado(self, __minutos_estacionado):
        self.__minutos_estacionado = int(__minutos_estacionado)

    def get_minutos_estacionado(self):
        return self.__minutos_estacionado
    
    def calculo_tarifa(self,a,b):
        return (a*b)
    
#clase donde se almacena el valor de las tarifas
class Tarifa:
    def __init__(self, __valor_tarifa):
        self.__valor_tarifa = float(__valor_tarifa)
    
    def get_valor_tarifa(self):
            return float(self.__valor_tarifa)
    
    def set_valor_tarifa(self,__valor_tarifa):
            self.__valor_tarifa = float(__valor_tarifa)


#clase que simula un servicio externo.
#podria tratarse de una API a la que solo consultariamos
#pero el ejercicio pide que el email se envie desde esta clase mediante un metodo estatico
class Servicio_externo:
    patente_valida_lista = [
        {'nombre':'persona1',
        'patente':'aaa111',
        'mail':'persona1@email.com'},
        {'nombre':'persona2',
        'patente':'bbb222',
        'mail':'persona2@email.com'},
        {'nombre':'persona3',
        'patente':'ccc333',
        'mail':'persona3@email.com'},
        {'nombre':'persona4',
        'patente':'ddd444',
        'mail':'persona4@email.com'},
        {'nombre':'persona5',
        'patente':'eee555',
        'mail':'persona5@email.com'}
    ]
    patente_existente = ["aaa111","bbb222","ccc333","ddd444","eee555"]
    @staticmethod
    def enviar_email(asunto, cuerpo, destinatario):
        print(asunto)
        print(cuerpo)
        print(destinatario)
    


#clase donde se almacena datos del titular actual del automovil
#de los datos sacados del Servicio Externo se puede crear instancia de cada titular
#con el fin de llevar un registro propio en caso de necesitarlo
class Titular:
    pass

def cuenta_horas(value):
    '''From seconds to Days;Hours:Minutes;Seconds'''

    valueD = (((value/365)/24)/60)
    Days = int (valueD)

    valueH = (valueD-Days)*365
    Hours = int(valueH)

    valueM = (valueH - Hours)*24
    Minutes = int(valueM)

    valueS = (valueM - Minutes)*60
    Seconds = int(valueS)

    return (Hours+1) #se suma 1 para redondear, como pide el ejercicio
def cuenta_minutos(value):
    '''From seconds to Days;Hours:Minutes;Seconds'''

    valueD = (((value/365)/24)/60)
    Days = int (valueD)

    valueH = (valueD-Days)*365
    Hours = int(valueH)

    valueM = (valueH - Hours)*24
    Minutes = int(valueM)

    valueS = (valueM - Minutes)*60
    Seconds = int(valueS)

    return (Minutes)




# instacias
parquimetro_1 = Parquimetro(2831)
tarifa1 = Tarifa(25.8)

## PROGRAMA PRINCIPAL ##

# variable para condicion de salida
salida = True


while(salida):
    #simulamos la deteccion de la patente ingresandola mediante el teclado
    patente = str(input("Ingrese la patente del auto, o 0 para salir:\n"))
    

    #condicion de salida del bucle
    if patente == '0':
        salida = False
        print("Cerrando programa...")

    #si hay un vehiculo estacionado:
    else:
        #verificacion patente
        patente_valida = False
        for i in range (len(Servicio_externo.patente_existente)):
            if (str(Servicio_externo.patente_existente[i]) == patente):
                patente_valida = True
                

        if (patente_valida):
            parquimetro_1.auto_detectado()
            parquimetro_1.set_patente_actual(patente)
            print(f"Automovil detectado, patente: {str(parquimetro_1.get_patente_actual())}\n")
            hoy = date.today()
            dia_actual = hoy.strftime("%d/%m/%Y")
            start = time.time()

            input("Presione una tecla cuando el automovil se retire\n")
            end = time.time() 
            print(f"HORAS ESTACIONADO: {cuenta_horas(end-start)}\n")
            parquimetro_1.set_minutos_estacionado(cuenta_minutos(end-start))
            parquimetro_1.estacionamiento_finalizado()

            #Notificacion de servicio externo
            Servicio_externo.enviar_email(f"Notificación del parquimetro: {parquimetro_1.get_id_parquimetro()}",f"Se notifica que el vehículo de patente: {parquimetro_1.get_patente_actual()}, estuvo {cuenta_horas(end-start)} horas estacionado en el día {dia_actual}.\nLa tarifa a abonar es de ${parquimetro_1.calculo_tarifa(cuenta_horas(end-start),tarifa1.get_valor_tarifa())}",f"Destinatario: email@email.com\n\n")
            #reinicia el contador para el proximo vehículo
            parquimetro_1.set_minutos_estacionado(0) 
            parquimetro_1.set_patente_actual("")
        
        else:
            print("Patente inválida. Notificando a las autoridades")
            Servicio_externo.enviar_email(f"Notificando patente inválida: {patente}",f"Se notifica que el vehículo de patente: {patente} se encuentra estacionado en el parquimetro {parquimetro_1.get_id_parquimetro()}.","regulacionDelAutomotor@email.com\n\n")

        
